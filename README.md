# Java Command Line Compile Example

![Made with Java](https://forthebadge.com/images/badges/made-with-java.svg)
![forthebadge](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)

An example Java project using the command line to compile.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You will need Java 1.8 installed configured on your system PATH  
It's best to use the [OpenJDK][openjdk], Windows users can get binaries from [AdoptOpenJDK][adoptopenjdk]

### Installing

1. Clone the repository with the name of your new project:  
   `git clone git@bitbucket.org:Skerwe/java-command-line-compile-example.git <project-name>`

2. Compile your application:  
   `compile.bat` or `./compile.sh`

3. Execute your application:  
   `run.bat` or `./run.sh`

## License

The source code is free -- see the [UNLICENSE](UNLICENSE) file for details

[openjdk]: https://openjdk.java.net/
[adoptopenjdk]: https://adoptopenjdk.net/
