package za.web.skerwe.code.example;

import java.util.Scanner;

public class Sample {

  public static void main(String[] args) {
    System.out.println("\nWelcome!\n");

    System.out.print("What is your name? ");
    Scanner input = new Scanner(System.in);
    String userInput = input.next();

    System.out.printf("Hello, %s!\n", userInput);
    input.close();
  }
}
